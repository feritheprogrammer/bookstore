# BookStore(کتاب‌فروشی)

I designed a Book Store website with Laravel. (Language is in Farsi)


## Properties(خصوصیات)
* Send Email and SMS for registering a user and adding a book.
* Implementing a complete resource controller for Book model.
* Authentication: Unregistered users have not access to 80% of website.
* Authorization: Every user can only edit and delete books of his/her own.
* API development alongside the website.
* A comprehensive collection of fake data will generate with seeders for the first use.
* ...


## How to run? (نحوه‌ی اجرا)
1. Make sure git and composer are installed.
2. Clone the repository.
3. Open cmd (or terminal), change directory to project folder, and run `composer update`
4. Enter `php artisan key:generate`.
5. Initialize parameters of .env file.
6. Enter `php artisan migrate --seed`
7. Serve the project, open the browser, and go to http://127.0.0.1:8000/
