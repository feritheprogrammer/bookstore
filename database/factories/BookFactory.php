<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Book;
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Book::class, function (Faker $faker) {

    $user = User::all()->random(1)->first();

    return [
        'name' => $faker->firstName('female'),
        'user_id' => $user->id,
        'pages' => $faker->numberBetween(100,1000),
        'ISBN' => Str::random(10),
        'price'=> $faker->numberBetween(10,100),
        'published_at' => $faker->date('Y-m-d'),
    ];
});
