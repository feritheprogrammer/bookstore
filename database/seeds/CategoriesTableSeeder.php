<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert(
            [
                ['name' => 'Western'],
                ['name' =>'Thriller'],
                ['name' =>'Romance'],
                ['name' =>'Mystery'],
                ['name' =>'Fantasy'],
                ['name' =>'Science fiction'],
                ['name' =>'Biography'],
                ['name' =>'Musical'],

            ]
        );
    }
}
