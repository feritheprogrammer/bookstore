<?php

use Illuminate\Database\Seeder;
use \App\Author;
use \App\Book;
use \App\Category;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Creating Books
        factory(Book::class, 10)
            ->create()
            ->each(function ($book) {
                // Filling Pivot Tables (author_book)
                $author = Author::pluck('id')->random(1)->first();
                $category = Category::pluck('id')->random(rand(1,3))->toArray();
//                DB::table('author_book')->insert(
//                    [
//                        'book_id' => $book->id,
//                        'author_id' => $author->id,
//                    ]
//                );
                $book->author()->attach($author);
                // Filling Pivot Tables (book_category)
//                DB::table('book_category')->insert(
//                    [
//                        'book_id' => $book->id,
//                        'category_id' => $category->id,
//                    ]
//                );
                $book->category()->attach($category);
            });
    }
}
