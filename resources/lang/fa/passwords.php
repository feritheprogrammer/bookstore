<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'رمز عبور شما به حالت ابتدایی بازگشت.',
    'sent' => 'لینک بازیابی رمز عبود به ایمیل شما ارسال شده است.',
    'token' => 'کد بازگشت رمز عبور به حالت اولیه نامعتبر است.',
    'user' => "کاربری با این آدرس ایمیل موجود نمی‌باشد.",

];
