@component('mail::message')
# ثبت‌ موفقیت‌آمیز کتاب در سیستم

کاربر گرامی
{{$book['user_name']}}

شما کتاب
{{$book['book_name']}}
را با موفقیت در سیستم به ثبت رساندید.
با تشکر<br>
تیم مدیریت سایت
@endcomponent
