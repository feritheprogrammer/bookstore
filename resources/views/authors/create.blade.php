@extends('layout')

@section('title', 'Add a new Author')

@section('content')
    <div class="row justify-content-center ">
        <div class="col-8 ">
            <h4 class="btn-primary text-center py-2 col-12" style="border-radius: 5px;font-family: Titr !important;">
                اضافه کردن یک نویسنده جدید
            </h4>
            <form class="farsi col-12" action="/authors" method="POST">
                @csrf
                <div class="form-group row">
                    <label for="name" class="col-sm-2 col-form-label ">نام نویسنده</label>
                    <div class="col-sm-10">
                        <input type="text" id="name" name="name"
                               class="form-control @error('name') is-invalid @enderror"
                               placeholder="نام" value="{{old('name')}}">
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="birthday" class="col-2 col-form-label">تاریخ تولد</label>
                    <div class="col-10">
                        <input type="text" id="birthday" name="birthday"
                               class="form-control @error('birthday') is-invalid @enderror"
                               placeholder="تاریخ تولد" value="{{old('birthday')}}">
                        @error('birthday')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{$message}}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row flex-row-reverse">
                    <div class="col-sm-10 text-left ">
                        <button type="submit" class="btn btn-success px-4">تأیید</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
