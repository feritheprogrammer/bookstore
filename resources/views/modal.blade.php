<!-- Modal 1-->
<div class="modal fade" id="Modal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog farsi" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title " id="exampleModalLabel">ارتباط با ما</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-left: 0">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>
                    طراح این وبسایت آقای فرنام صمدی می‌باشد که به طریق زیر می‌توانید با ایشان در
                    ارتباط باشید.
                </p>
                <p class="text-left">fm.samadi70@gmail.com</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal 2-->
<div class="modal fade" id="Modal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog farsi" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title" id="exampleModalLabel">درباره‌ی ما</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-left: 0">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>
                    طراح این وبسایت آقای فرنام صمدی می‌باشد که به طریق زیر می‌توانید با ایشان در
                    ارتباط باشید.
                </p>
                <p class="text-left">fm.samadi70@gmail.com</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>
            </div>
        </div>
    </div>
</div>

