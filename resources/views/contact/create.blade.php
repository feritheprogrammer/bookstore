@extends('layout')

@section('title', 'ارتباط با ما')

@section('content')
    @if(session()->has('message'))
        <div class="alert alert-success farsi" role="alert">
            <strong>موفقیت‌آمیز</strong> {{session()->get('message')}}
        </div>
    @endif
    <form action="/contact" method="POST" class="farsi">
        <div class="form-group row">
            <label for="name" class="col-sm-2 col-form-label">
                <strong>
                نام:
                </strong>
            </label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="name" name="name" value="{{old('name')}}">
            </div>
        </div>
        <div class="form-group row">
            <label for="email" class="col-sm-2 col-form-label">
                <strong>
                    ایمیل:
                </strong>
            </label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="email" name="email" value="{{old('email')}}">
            </div>
        </div>
        <div class="form-group row">
            <label for="message" class="col-sm-2 col-form-label">
               <strong>متن پیام:</strong>
            </label>
            <div class="col-sm-10">
                <textarea name="message" id="message" cols="30" rows="10" class="form-control" value="{{old('message')}}">
                </textarea>
            </div>
        </div>
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
                </ul>
            </div>
        @endif
        @csrf
        <div class="form-group row flex-row-reverse text-left">
            <button type="submit" class="btn btn-primary px-4 ml-3">ارسال</button>
        </div>
    </form>
@endsection
