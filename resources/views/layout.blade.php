<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <!-- Importing Styles -->
    <link type="text/css" rel="stylesheet" href="{{asset('css/reset.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('css/style.css')}}"/>
    <title>
        @yield('title')
    </title>
</head>
<body class="d-flex flex-column h-100">
<header>
    @include('nav')
    @include('modal')
</header>
<div class="container mt-3 " style="padding-top: 60px">
    @yield('content')
</div>
<footer class="footer py-3 bg-secondary mt-auto farsi" style="position: relative">
    <div class="container text-center">
        <span class="text-light ">این کد توسط فرنام صمدی نوشته شده است.</span>
    </div>
</footer>

<!-- Importing Javascript -->
<script src="{{asset('js/jquery-3.4.1.min.js')}}"></script>
<script src="{{asset('js/popper.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
</body>
</html>
