@extends('layout')

@section('title', 'پروژه‌ی فصل 7')

@section('content')
    @if($message = Session::get('success'))
    <div class="farsi alert alert-success alert-dismissible fade show">
        <strong> {{$message}}</strong>
        <button type="button" class="close" data-dismiss="alert" style="right: auto;left: 0">&times;</button>
    </div>
    @endif
    <div class="container d-flex flex-wrap" style="position:relative;">
        @foreach($books as $book)
            <div class="card text-center mx-2 my-2" style="width: 15rem;">
                <div class="card-body mt-2">
                    <h3 class="card-title">{{$book->name}}</h3>
                    <p class="card-text">{{$book->ISBN}}</p>
                    <a href="/books/{{$book->id}}" class="btn btn-info">View book</a>
                </div>
            </div>
        @endforeach
    </div>
@endsection
