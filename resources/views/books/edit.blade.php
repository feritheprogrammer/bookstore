@extends('layout')

@section('title', 'اصلاح کتاب')

@section('content')
    <div class="row justify-content-center ">
        <div class="col-8 ">
            <h4 class="btn-primary text-center py-2 col-12" style="border-radius: 5px;font-family: Titr !important;">
                اصلاح اطلاعات کتاب
            </h4>
            <form class="farsi col-12" action="/books/{{$book->id}}" method="POST">
                @method('PATCH')
                @csrf
                <div class="form-group row">
                    <label for="name" class="col-sm-2 col-form-label ">نام کتاب</label>
                    <div class="col-10">
                        <input type="text" id="name" name="name"
                               class="form-control @error('name') is-invalid @enderror"
                               placeholder="نام" value="{{old('name') ?? $book->name}}">
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="author" class="col-sm-2 col-form-label">نویسنده</label>
                    <div class="col-10">
                        <select class="custom-select @error('author') is-invalid @enderror" multiple
                                name="author[]" id="author">
                            @foreach($authors as $author)
                                <option value="{{$author->id}}"
                                    {{$book->author->contains('id', $author->id) ? 'selected' : ''}}>
                                    {{$author->name}}
                                </option>
                            @endforeach
                        </select>
                        @error('author')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{$message}}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="pages" class="col-2 col-form-label ">تعداد صفحات</label>
                    <div class="col-10">
                        <input type="text" id="pages" name="pages"
                               class="form-control @error('pages') is-invalid @enderror"
                               placeholder="تعداد صفحات" value="{{old('pages')??$book->pages}}">
                        @error('pages')
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="ISBN" class="col-sm-2 col-form-label ">شابک</label>
                    <div class="col-sm-10">
                        <input type="text" id="ISBN" name="ISBN"
                               class="form-control @error('ISBN') is-invalid @enderror"
                               placeholder="شابک" value="{{old('ISBN')??$book->ISBN}}">
                        @error('ISBN')
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="price" class="col-sm-2 col-form-label ">قیمت($)</label>
                    <div class="col-sm-10">
                        <input type="text" id="price" name="price"
                               class="form-control @error('price') is-invalid @enderror"
                               placeholder="قیمت" value="{{old('price')??$book->price}}">
                        @error('price')
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="published_at" class="col-sm-2 col-form-label">تاریخ انتشار</label>
                    <div class="col-sm-10">
                        <input type="text" id="published_at" name="published_at"
                               class="form-control @error('published_at') is-invalid @enderror"
                               placeholder="منتشر شده در تاریخ" value="{{old('published_at') ?? $book->published_at}}">
                        @error('published_at')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{$message}}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="category" class="col-sm-2 col-form-label">ژانر کتاب</label>
                    <div class="col-10">
                        <select class="custom-select @error('category_id') is-invalid @enderror" multiple
                                name="category_id[]" id="category">
                            @foreach($categories as $category)
                                <option value="{{$category->id}}"
                                    {{$book->category->contains('name',$category->name) ? 'selected' : ''}}>
                                    {{$category->name}}
                                </option>
                            @endforeach
                        </select>
                        @error('category_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{$message}}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                {{--        @include('errors')--}}
                <div class="form-group row flex-row-reverse">
                    <div class="col-10 text-left ">
                        <button type="submit" class="btn btn-warning px-4">ذخیره تغییرات</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
