@extends('layout')

@section('title', $book->name)

@section('content')
    <div class="jumbotron " style="position:relative;">
        <div class="container">
            <h1 class="display-4">{{$book->name}}</h1>
            <h4> Author:
                @foreach($book->author as $author)
                    {{$author->name}}
                @endforeach
            </h4>
            <h4>Price: {{$book->price}}$</h4>
            <h4>Pages: {{$book->pages}}</h4>
            <h4>ISBN: {{$book->ISBN}}</h4>
            <h4>Published at: {{date_format(date_create($book->published_at),"F j, Y")}}</h4>
            <h4>Category:
                @foreach($book->category as $category)
                    {{$category->name}}
                @endforeach
            </h4>

            <br>
            <br>
            <h4>Added by user: {{$book->user->name}}</h4>
            <div class="row justify-content-start">
                <div class="col-1">
                    @can('update',$book)
                        <a href="/books/{{$book->id}}/edit" class="btn btn-primary px-4 mt-3">Edit</a>
                    @else
                        <a href="/books/{{$book->id}}/edit" class="btn btn-muted px-4 mt-3 disabled">Edit</a>
                    @endcan
                </div>
                <div class="col-1">
                    @can('delete', $book)
                        <a class="btn btn-danger px-4 mt-3" data-toggle="modal" data-target="#ModalConfirm" href="">
                            Delete
                        </a>
                    @else
                        <a class="btn btn-muted disabled px-4 mt-3 " data-toggle="modal" data-target="#ModalConfirm" href="">
                            Delete
                        </a>
                    @endcan
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade"
         id="ModalConfirm"
         tabindex="-1"
         role="dialog"
         aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog farsi" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title" id="exampleModalLabel">آیا از حذف کتاب مطمئن هستید؟</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-left: 0">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-footer ">
                    <div class="row">
                        <div class="col-2 p-1 mx-2">
                            <button type="button" class="btn btn-secondary " data-dismiss="modal">خیر</button>
                        </div>
                        <div class="col-2 p-1 mx-3">
                            <form action="/books/{{$book->id}}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-danger ">بلی</button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
