@extends('layout')

@section('title', 'خطای 403')

@section('content')
    <div class="row justify-content-center">
        <p class="farsi text-center">
            با عرض پوزش دسترسی شما به این بخش از سایت امکان‌پذیر نمی‌باشد! <br>
             برای دسترسی می‌توانید در سایت
            <a href="{{route('register')}}">ثبت‌نام</a>
            نمایید یا اگر عضو هستید
            <a href="{{route('login')}}">
                وارد شوید.
            </a>
        </p>
    </div>
@endsection
