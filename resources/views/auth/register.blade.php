@extends('layout')

@section('title', 'ثبت‌نام در سایت')

@section('content')
    <div class="row justify-content-center">
        <div class="col-8">
            <div class="card">
                <div class="card-header farsi text-center"><h3>ثبت‌نام در سایت</h3></div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row justify-content-center" style="direction:rtl">
                            <div class="row col-2">
                                <label for="name" class="col-form-label">نام</label>
                            </div>
                            <div class="col-6">
                                <input id="name" type="text" style="direction: ltr"
                                       class="form-control @error('name') is-invalid @enderror"
                                       name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row justify-content-center" style="direction:rtl">
                            <div class="row col-2">
                                <label for="email" class="col-form-label">ایمیل</label>
                            </div>
                            <div class="col-6">
                                <input id="email" type="email" style="direction: ltr"
                                       class="form-control @error('email') is-invalid @enderror"
                                       name="email"
                                       value="{{ old('email') }}"
                                       required
                                       autocomplete="email"
                                       autofocus>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row justify-content-center" style="direction:rtl">
                            <div class="row col-2">
                                <label for="national_code" class="col-form-label">کد ملی</label>
                            </div>
                            <div class="col-6">
                                <input id="national_code" type="text" style="direction: ltr"
                                       class="form-control @error('national_code') is-invalid @enderror"
                                       name="national_code" value="{{ old('national_code') }}" required
                                       autocomplete="national_code" autofocus>

                                @error('national_code')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row justify-content-center" style="direction: rtl">
                            <div class="row col-2 justify-content-start">
                                <label for="phone_number" class=" col-form-label">شماره موبایل</label>
                            </div>
                            <div class="col-6">
                                <input id="phone_number" style="direction: ltr" type="text"
                                       class="form-control @error('phone_number') is-invalid @enderror"
                                       name="phone_number" value="{{ old('phone_number') }}" required
                                       autocomplete="email">

                                @error('phone_number')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row justify-content-center" style="direction: rtl">
                            <div class="row col-2">
                                <label for="password" class=" col-form-label ">رمز عبور</label>
                            </div>

                            <div class="col-6">
                                <input id="password" type="password" style="direction: ltr"
                                       class="form-control @error('password') is-invalid @enderror" name="password"
                                       required autocomplete="new-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row justify-content-center" style="direction: rtl">
                            <div class="col-2 row">
                                <label for="password-confirm" class="col-form-label">تکرار رمز عبور</label>
                            </div>
                            <div class="col-6">
                                <input id="password-confirm" style="direction: ltr" type="password" class="form-control"
                                       name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row justify-content-center mb-0" style="direction: rtl">
                            <div class="col-8 row justify-content-end ml-3">
                                <button type="submit" class="btn btn-primary">
                                    ثبت‌نام
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
