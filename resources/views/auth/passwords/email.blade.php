@extends('layout')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h3 class="text-center">
                        بازیابی رمز عبور
                    </h3>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row justify-content-center"
                             style="direction:rtl">
                            <label for="phone_number"
                                   class="col-2 col-form-label text-md-right">شماره موبایل</label>

                            <div class="col-md-6">
                                <input id="phone_number" type="text" style="direction: ltr"
                                       class="form-control @error('phone_number') is-invalid @enderror"
                                       name="phone_number" value="{{ old('phone_number') }}"
                                       required
                                       autocomplete="phone_number" autofocus>

                                @error('phone_number')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row justify-content-center mb-0" style="direction: rtl">
                            <div class="col-9 row justify-content-end">
                                <div class="col-6 ml-3">
                                    <button type="submit" class="btn btn-primary">
                                        ارسال کد بازیابی رمز عبور
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
