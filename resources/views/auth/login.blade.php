@extends('layout')

@section('title','ورود')

@section('content')
    <div class="row justify-content-center">
        <div class="col-8">
            <div class="card">
                <div class="card-header farsi text-center"><h3>ورود به سایت</h3></div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row" style="direction:rtl">
                            <label for="phone_number"
                                   class="col-4 col-form-label">شماره‌ی موبایل</label>

                            <div class="col-6">
                                <input id="phone_number" type="text" style="direction: ltr"
                                       class="form-control @error('phone_number') is-invalid @enderror"
                                       name="phone_number" value="{{ old('phone_number') }}" required autocomplete="phone_number" autofocus placeholder="09xxxxxxxxx">

                                @error('phone_number')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row" style="direction: rtl">
                            <label for="password"
                                   class="col-md-4 col-form-label">رمز عبور</label>

                            <div class="col-md-6">
                                <input id="password" type="password" style="direction: ltr"
                                       class="form-control @error('password') is-invalid @enderror" name="password"
                                       required autocomplete="current-password" placeholder="رمزعبور">

                                @error('password')
                                <span class="invalid-feedback" style="direction: rtl" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row justify-content-center" style="direction: rtl">
                            <div class="col-4">
                                <div class="form-check text-right">
                                    <input class="form-check-input" type="checkbox" name="remember"
                                           id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label mr-3" for="remember">
                                        مرا به یاد داشته باش
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row justify-content-center mb-0" style="direction: rtl">
                            <div class="col-4 row justify-content-start">
                                <button type="submit" class="btn btn-primary">
                                    ورود
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        فراموشی رمز عبور
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
