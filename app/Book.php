<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }

    public function category()
    {
        return $this->belongsToMany(\App\Category::class);
    }

    public function author()
    {
        return $this->belongsToMany(\App\Author::class);
    }
}

