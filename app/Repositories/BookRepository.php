<?php

namespace App\Repositories;

use App\Book;
use App\Events\NewBookHasCreatedEvent;
use App\Http\Requests\BookRequest;
use Illuminate\Support\Facades\Auth;

class BookRepository
{
    protected $book;

    public function __construct(Book $book)
    {
        $this->book = $book;
    }

    public function storeBook(BookRequest $request)
    {
        $request->validated();

        $user = Auth::user();

        $book = $user->books()->create($request->except('_token', 'category_id', 'author'));
        $book->category()->sync($request->category_id);
        $book->author()->sync($request->author);

//        event(new NewBookHasCreatedEvent($book));

        return 'کتاب شما با موفقیت در سیستم ثبت گردید.';
    }

    public function updateBook(BookRequest $request, Book $book)
    {
        $request->validated();

        $book->update($request->except('author', 'category_id'));
        $book->category()->sync($request->category_id);
        $book->author()->sync($request->author);

        return 'تغییرات موردنظر با موفقیت اعمال شد.';
    }

    public function deleteBook(Book $book)
    {
        $book->author()->detach();
        $book->category()->detach();
        $book->delete();

        return 'کتاب مورد نظر با موفقیت از سیستم حذف گردید.';
    }
}
