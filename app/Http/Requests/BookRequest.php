<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:256',
            'author' => 'required',
            'pages' => 'required|integer|min:0',
            'ISBN' => 'required|size:10',
            'price' => 'required|integer',
            'published_at' => 'required|date',
            'category_id' => 'required',
        ];
    }
}
