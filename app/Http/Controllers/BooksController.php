<?php

namespace App\Http\Controllers;

use App\Author;
use App\Book;
use App\Category;
use App\Http\Requests\BookRequest;
use App\Repositories\BookRepository;

class BooksController extends Controller
{
    /**
     * @var BookRepository
     */
    private $bookRepository;

    public function __construct(BookRepository $bookRepository)
    {
        $this->bookRepository = $bookRepository;

        $this->middleware('auth')->only('create', 'edit', 'destroy');
    }

    public function index()
    {
        $books = Book::all();
        return view('books.index', compact('books'));
    }

    public function create()
    {
        $authors = Author::all();
        $categories = Category::all();

        return view('books.create', compact('categories', 'authors'));
    }

    public function show(Book $book)
    {
        return view('books.show', compact('book'));
    }

    public function store(BookRequest $request)
    {
        $ans = $this->bookRepository->storeBook($request);

        return redirect('/books')->with('success', $ans);
    }

    public function edit(Book $book)
    {
        $authors = Author::all();
        $categories = Category::all();

        return view('books.edit', compact(['book', 'authors', 'categories']));
    }

    public function update(BookRequest $request, Book $book)
    {
        $ans = $this->bookRepository->updateBook($request,$book);

        return redirect('/books')->with('success', $ans);
    }

    public function destroy(Book $book)
    {
        $ans = $this->bookRepository->deleteBook($book);

        return redirect('/books')->with('success', $ans);
    }
}
