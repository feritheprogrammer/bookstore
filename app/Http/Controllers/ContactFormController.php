<?php

namespace App\Http\Controllers;

use App\Mail\ContactFormMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactFormController extends Controller
{
    public function create()
    {
        return view('contact.create');
    }

    public function store()
    {
        $validationData = request()->validate([
            'name' => 'required|string|min:3',
            'email' => 'required|email',
            'message' => 'required|string'
        ]);
        Mail::to('test@test.com')->send(new ContactFormMail($validationData));

        return redirect('contact')->with('message','پیام شما با موفقیت دریافت شد. منتظر تماس ما باشید.');
    }
}
