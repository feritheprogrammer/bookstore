<?php

namespace App\Http\Controllers;

use App\Author;
use Illuminate\Http\Request;

class AuthorsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->only('create');
    }
    public function create()
    {
        return view('authors.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'birthday' => 'required|date',
         ]);

        Author::create($request->except('_token'));

        return redirect('/books')->with('success', 'اطلاعات نویسنده با موفقیت در سیستم ثبت گردید.');
    }
}
