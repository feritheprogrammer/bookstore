<?php

namespace App\Listeners;

use App\Events\NewUserHasRegisteredEvent;
use App\Mail\UserRegisteredMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendEmailWelcomeNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewUserHasRegisteredEvent  $event
     * @return void
     */
    public function handle(NewUserHasRegisteredEvent $event)
    {
        $data = [
            'name' => $event->newUser->name,
            'email' => $event->newUser->email,
        ];

        Mail::to($data['email'])->send(new UserRegisteredMail($data));
    }
}
