<?php

namespace App\Listeners;

use App\Events\NewBookHasCreatedEvent;
use App\Mail\BookCreatedMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class SendEmailNewBookNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewBookHasCreatedEvent  $event
     * @return void
     */
    public function handle(NewBookHasCreatedEvent $event)
    {
        $book = [
            'book_name' => $event->book->name,
            'user_name' => Auth::user()->name,
            'email' => Auth::user()->email,
        ];

        Mail::to($book['email'])->send(new BookCreatedMail($book));
    }
}
