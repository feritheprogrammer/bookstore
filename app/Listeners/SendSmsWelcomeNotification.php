<?php

namespace App\Listeners;

use App\Events\NewUserHasRegisteredEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Kavenegar\KavenegarApi;

class SendSmsWelcomeNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewUserHasRegisteredEvent  $event
     * @return void
     */
    public function handle(NewUserHasRegisteredEvent $event)
    {
        $name = $event->newUser->name;
        $phone_number = $event->newUser->phone_number;

        $message = " سلام$name
        ثبت‌نام شما با موفقیت انجام پذیرفت.";

        $client = new KavenegarApi(env('KAVE_NEGAR_API_KEY'));
        $client->send(env('SENDER_MOBILE'), $phone_number , $message);
    }
}
