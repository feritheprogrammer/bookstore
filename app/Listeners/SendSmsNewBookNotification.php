<?php

namespace App\Listeners;

use App\Events\NewBookHasCreatedEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;
use Kavenegar\KavenegarApi;

class SendSmsNewBookNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param NewBookHasCreatedEvent $event
     * @return void
     */
    public function handle(NewBookHasCreatedEvent $event)
    {
        $name = Auth::user()->name;
        $phone_number = Auth::user()->phone_number;
        $book = $event->book->name;

        $message = " کاربر گرامی$name
        شما کتاب $book را با موفقیت در سیستم به ثبت رساندید.";

        $client = new KavenegarApi(env('KAVE_NEGAR_API_KEY'));
        $client->send(env('SENDER_MOBILE'), $phone_number , $message);
    }
}
