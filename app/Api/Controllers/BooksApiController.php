<?php

namespace App\Api\Controllers;

use App\Book;
use App\Http\Controllers\Controller;
use App\Http\Requests\BookRequest;
use App\Repositories\BookRepository;

class BooksApiController extends Controller
{
    private $bookRepository;

    public function __construct(BookRepository $bookRepository)
    {
        $this->bookRepository = $bookRepository;

        $this->middleware('auth.basic')->only('store', 'destroy', 'update');
    }

    public function index()
    {
        $books = Book::all();
        return $books;
    }

    public function store(BookRequest $request)
    {
        return $this->bookRepository->storeBook($request);
    }

    public function show(Book $book)
    {
        return $book;
    }

    public function update(BookRequest $request, Book $book)
    {
        return $this->bookRepository->updateBook($request,$book);
    }

    public function destroy(Book $book)
    {
        return $this->bookRepository->deleteBook($book);
    }
}
