<?php

namespace App\Providers;

use App\Events\NewBookHasCreatedEvent;
use App\Events\NewUserHasRegisteredEvent;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        NewUserHasRegisteredEvent::class => [
            \App\Listeners\SendSmsWelcomeNotification::class,
            \App\Listeners\SendEmailWelcomeNotification::class,
        ],
        NewBookHasCreatedEvent::class => [
            \App\Listeners\SendSmsNewBookNotification::class,
            \App\Listeners\SendEmailNewBookNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
