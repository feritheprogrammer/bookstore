<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    protected $guarded = [];
    protected $attributes = [
        'birthday' => '1999-01-01',
    ];

    public function book()
    {
        return $this->belongsToMany(\App\Book::class);
    }
}
