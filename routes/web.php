<?php

Route::view('/', 'welcome');
Route::get('/contact','ContactFormController@create');
Route::post('/contact','ContactFormController@store');

// Chapter 8 Quiz

// Book
Route::resource('/books', 'BooksController');


// Author
Route::resource('/authors', 'AuthorsController')
    ->only('create', 'store');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
